from google.appengine.ext import ndb 

class Mensaje (ndb.Model):
    origen = ndb.StringProperty()
    destino = ndb.StringProperty()
    contenido = ndb.StringProperty()
    imagen = ndb.StringProperty()
    fechaCreacion = ndb.DateTimeProperty(auto_now_add=True)
    
class Evento (ndb.Model):
    Creador = ndb.StringProperty()
    Nombre = ndb.StringProperty()
    Descripcion = ndb.StringProperty()
    Longitud = ndb.StringProperty()
    Latitud = ndb.StringProperty()
    idJuego = ndb.StringProperty()

    #Ganador = db.StringProperty()
    
class Juego (ndb.Model):
    Creador = ndb.StringProperty()
    Nombre = ndb.StringProperty()
    Descripcion = ndb.StringProperty()
    
class EventoGanador(ndb.Model):
    idJuego = ndb.StringProperty()
    idEvento = ndb.StringProperty()
    Ganador = ndb.StringProperty()
    
    

