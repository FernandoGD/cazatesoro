from views import acceder, crearEvento, mostrarEmails, responderMensajes,\
    verImagen,EditEvento, EliminarEvento, PistaEvento, crearJuego, eventos, EliminarJuego,\
    EditJuego, ganador, addGanador, ganadorGif
import webapp2


app = webapp2.WSGIApplication([
        ('/', acceder),
        ('/misMensajes', mostrarEmails),  
        ('/crearEvento/([\d]+)', crearEvento),
        ('/crearJuego', crearJuego),
        ('/eventos/([\d]+)', eventos),
        ('/editEvento/([\d]+)', EditEvento),
        ('/editJuego/([\d]+)', EditJuego),
        ('/editEvento/([\d]+)/eliminar', EliminarEvento),
        ('/editJuego/([\d]+)/eliminar', EliminarJuego),
        ('/verImagen/([\d]+)', verImagen),
        ('/pistaEvento/([\d]+)', PistaEvento),
        ('/ganador/([\d]+)/([\d]+)', ganador),
        ('/addGanador/([\d]+)/([\d]+)', addGanador),
        ('/tenemosGanador', ganadorGif),
        
        ],
        debug=True)
