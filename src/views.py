from datetime import datetime
import os
import webapp2
import jinja2
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.api import search

from models import Mensaje, Evento, Juego, EventoGanador
from _warnings import filters

TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')
jinja_environment = \
    jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR))


class BaseHandler(webapp2.RequestHandler):

    def render_template(
        self,
        filename,
        template_values,
        **template_args
        ):
        template = jinja_environment.get_template(filename)
        self.response.out.write(template.render(template_values))

    def tokenize_autocomplete(self, phrase):
        a = []
        for word in phrase.split():
            j = 1
            while True:
                for i in range(len(word) - j + 1):
                    a.append(word[i:i + j])
                if j == len(word):
                    break
                j += 1
        return a

class acceder(BaseHandler):
    def get(self):
        user= users.get_current_user().nickname()
        log_out=users.create_logout_url('/')
        juegos = Juego.query()
        self.render_template('Inicio.html', {'user' :user, 'log_out':log_out, 'juegos':juegos })
        
class eventos(BaseHandler):
        
    def get(self, juego_id):
        id = int(juego_id)
        user= users.get_current_user().nickname()
        eventos = Evento.query().filter(Evento.idJuego==juego_id)
        ganador = EventoGanador.query().filter(EventoGanador.Ganador==user)
        juego = ndb.Key('Juego', id).get()
        log_out=users.create_logout_url('/')
        #eventos = Evento.all()
        self.render_template('eventos.html', {'eventos': eventos, 'juego':juego, 'user':user, 'log_out':log_out, 'ganador':ganador})
        
class ganadorGif(BaseHandler):        
    def get(self):
        user= users.get_current_user().nickname()
        self.render_template('tenemosGanador.html', {'user':user})
        
class crearEvento(BaseHandler):
    
    def post(self,id_juego):
        evento = Evento(    Creador = users.get_current_user().nickname(),
                            Nombre = self.request.get('inputNombreEvento'),
                            Descripcion =self.request.get('inputDescripcion'),
                            Longitud = self.request.get('inputLongitud'),
                            Latitud = self.request.get('inputLatitud'),
                            idJuego = id_juego)
        evento.put()
        return webapp2.redirect('/')
    
    def get(self,id_juego):
        id = int(id_juego)
        juego = ndb.Key('Juego', id).get()
        self.render_template('crearEvento.html', {'juego':juego})
class crearJuego(BaseHandler):
    
    def post(self):
        juego = Juego(     Creador = users.get_current_user().nickname(),
                            Nombre = self.request.get('inputNombreJuego'),
                            Descripcion =self.request.get('inputDescripcionJuego'))
        juego.put()
        return webapp2.redirect('/')
    
    def get(self):
        self.render_template('crearJuego.html', {})
        
class mostrarEmails(BaseHandler):
    def get (self):
        mensajes = Mensaje.all()
        self.render_template('misMensajes.html', {'mensajes' : mensajes}) 
        
class EditEvento(BaseHandler):
    
    def post(self, evento_id):
        idEvento = int(evento_id)
        evento = ndb.Key('Evento', idEvento).get()
        evento.Nombre = self.request.get('inputNombreEvento')
        evento.Descripcion =self.request.get('inputDescripcion')
        evento.Estado = "Activo"
        evento.Longitud = self.request.get('inputLongitud')
        evento.Latitud = self.request.get('inputLatitud')
        evento.Ganador = self.request.get('inputGanador')
        evento.put()
        return webapp2.redirect('/')
    
    def get(self, evento_id):
        idEvento = int(evento_id)
        evento = ndb.Key('Evento', idEvento).get()
        ganadores = EventoGanador.query().filter(EventoGanador.idEvento==evento_id)
        self.render_template('editEvento.html', {'evento': evento, 'ganadores':ganadores})
        
class EditJuego(BaseHandler):
    
    def post(self, juego_id):
        idJuego = int(juego_id)
        juego = ndb.Key('Juego', idJuego).get()
        juego.Nombre = self.request.get('inputNombreJuego')
        juego.Descripcion =self.request.get('inputDescripcion')
        juego.put()
        return webapp2.redirect('/')
    
    def get(self, juego_id):
        idJuego = int(juego_id)
        juego = ndb.Key('Juego', idJuego).get()
        self.render_template('editJuego.html', {'juego': juego})
        
class EliminarEvento(BaseHandler):
    
    def get(self, evento_id):
        idEvento = int(evento_id)
        evento = ndb.Key('Evento', idEvento).delete()
        return webapp2.redirect('/')
    
class EliminarJuego(BaseHandler):
    
    def get(self, juego_id):
        idJuego = int(juego_id)
        juego = ndb.Key('Juego', idJuego).get()
        eventos = Evento.query().filter(Evento.idJuego==juego_id).fetch(keys_only=True)
        ndb.delete_multi(eventos)
        ndb.Key('Juego', idJuego).delete()
        return webapp2.redirect('/')
    
class addGanador(BaseHandler):
    
    def get(self, juego_id,evento_id):
        iden = int(evento_id)
        idenJuego = int (juego_id)
        evento = ndb.Key('Evento', iden).get()
        juego = ndb.Key('Juego', idenJuego).get()
        user= users.get_current_user().nickname()
        eventosTotalJuego = Evento.query().filter(Evento.idJuego==juego_id)
        if EventoGanador.query().filter(EventoGanador.idEvento==evento_id, EventoGanador.Ganador==user).count() == 0:
            eventoGanador = EventoGanador(idJuego = evento.idJuego, idEvento = evento_id, Ganador = user)
            eventoGanador.put()
            eventosGanadosUser = EventoGanador.query().filter(EventoGanador.idJuego==juego_id, EventoGanador.Ganador == user)
        #print(eventosTotalJuego.count())
        #print(eventosGanadosUser.count())
            if eventosTotalJuego.count() == eventosGanadosUser.count()+1:
                eventos = Evento.query().filter(Evento.idJuego==juego_id).fetch(keys_only=True)
                eventosG = EventoGanador.query().filter(EventoGanador.Ganador == user, EventoGanador.idJuego ==juego_id).fetch(keys_only=True)
                ndb.delete_multi(eventos)
                ndb.delete_multi(eventosG)
                ndb.Key('Juego', idenJuego).delete()
                eventoGanador.key.delete()
                return webapp2.redirect('/tenemosGanador')
                
            else:
                return webapp2.redirect('/')
        return webapp2.redirect('/') 
                    
        
class responderMensajes(BaseHandler):
    def post(self, mensaje_id):         
        iden = int(mensaje_id)
        mensaje = ndb.get(ndb.Key.from_path('Mensaje', iden))
        mensaje.destino = self.request.get('inputOrigen')
        mensaje.origen =self.request.get('inputDestino')
        mensaje.imagen = self.request.get('inputImageId')
        mensaje.contenido = self.request.get('inputTextoRespuesta')
        mensaje2=Mensaje()
        mensaje2.origen=mensaje.origen
        mensaje2.destino=mensaje.destino
        mensaje2.contenido=mensaje.contenido
        mensaje2.imagen=mensaje.imagen
        mensaje2.put()
        return webapp2.redirect('/misMensajes')

    def get(self, mensaje_id):       
        iden = int(mensaje_id)
        mensaje = ndb.get(ndb.Key.from_path('Mensaje', iden))
        self.render_template('responderMensaje.html', {'mensaje' : mensaje})
        
class verImagen(BaseHandler):
     def get(self, mensaje_id):       
        iden = int(mensaje_id)
        mensaje = ndb.get(ndb.Key.from_path('Mensaje', iden))
        self.render_template('responderMensaje.html', {'mensaje' : mensaje})
        
        
class PistaEvento(BaseHandler):
     def get(self, evento_id):       
        iden = int(evento_id)
        evento = ndb.Key('Evento', iden).get()
        self.render_template('pistaEvento.html', {'evento' : evento})
        
class ganador(BaseHandler):
     def get(self,juego_id, evento_id):       
        iden = int(evento_id)
        idenJuego = int(juego_id)
        evento = ndb.Key('Evento', iden).get()
        juego = ndb.Key('Juego', idenJuego).get()
        self.render_template('ganador.html', {'evento' : evento, 'juego':juego})
        